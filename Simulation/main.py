import lightSimulation, constant, cubesat as cb
import numpy
import math
import matplotlib.pyplot as plt

print("Earth radius: %d m   Sun radius: %d m   Solar constant: %d W/m^2" % (constant.RADIUS_EARTH, constant.RADIUS_SUN, constant.SOLAR_CONSTANT))

pannels = [cb.Solarpannel(1,numpy.array([-1,0,0])), cb.Solarpannel(1,numpy.array([-1,-1,0])), cb.Solarpannel(1,numpy.array([1,0,0]))]
sat = cb.Cubesat(pannels)
print("cubesat created")




while True:
    x1 = constant.RADIUS_EARTH + float(input("Circular orbit height (in m above seelevel): "))
    res = int(input("resolution (number of measurements made to create graph): "))

    rot_x = 0
    rot_x /= res
    rot_x = numpy.array([[1, 0, 0],
                        [0, math.cos(rot_x), - math.sin(rot_x)],
                        [0, math.sin(rot_x), math.cos(rot_x)]])

    rot_y = 2
    rot_y /= res
    rot_y = numpy.array([[math.cos(rot_y), 0, math.sin(rot_y)],
                         [0, 1, 0],
                         [- math.sin(rot_y), 0, math.cos(rot_y)]])

    rot_z = -5
    rot_z /= res
    rot_z = numpy.array([[ math.cos(rot_z), - math.sin(rot_z), 0],
                        [math.sin(rot_z), math.cos(rot_z), 0],
                        [0, 0, 1]])

    rot = numpy.dot(numpy.dot(rot_x, rot_y), rot_z)

    print(rot)

    x = []
    shadow = []
    pannelPower = [[] for i in sat.solarpannels]
    print(pannelPower)
    power = []
    t_0 = 0
    dt = 1
    for i in range(res + 1):
        t = t_0 + dt * i
        x.append(i*(2*math.pi/res))
        position_cube = numpy.array([math.sin(x[i])*(x1),math.cos(x[i])*(x1),0])
        temp_cube = 0
        position_sun = numpy.array([-149600000000,0,0])

        state = [t, dt, position_cube, temp_cube, position_sun]

        #to calculate for whole cubesat
        light = constant.SOLAR_CONSTANT * lightSimulation.shadowCalc(state) #light on rhe satellite (0 ... 1)

        j = 0
        sumPower = 0
        for i in sat.solarpannels:
            i.dir = numpy.dot(rot, i.dir)
            val = light * i.area
            val *= lightSimulation.solarpannelAngle(state, i)
            val *= lightSimulation.solarpannelLoss(state, i)
            pannelPower[j].append(val)
            sumPower += val
            j += 1
        power.append(sumPower)









        shadow.append(light)



    print("percentage: " + str(sum(shadow)/ constant.SOLAR_CONSTANT / res))
    for i in pannelPower:
        plt.plot(x,i)
    plt.plot(x,shadow)
    plt.plot(x,power)
    plt.show()
