import constant
import numpy

def shadowCalc(state):

    [_, _, positionObj, _, positionSun] = state

    #for explanation of calculation refer to NASA Technical Paper 3547

    dp = constant.RADIUS_EARTH * 2
    ds = constant.RADIUS_SUN * 2
    dps = numpy.linalg.norm(positionSun)
    sn = positionSun/dps
    if (numpy.dot(positionObj,sn) < 0): #defenetly in sun
        return 1
    # xu and au of umbra
    xu = (dp * dps)/(ds - dp)
    au = numpy.arcsin(dp/(2* xu))

    # xp and ap of penumbra
    xp = (dp * dps)/(ds + dp)
    ap = numpy.arcsin(dp/(2* xp))

    #rs
    rs = numpy.dot(positionObj,sn)*sn

    #h
    h = numpy.linalg.norm(positionObj - rs)

    #k
    k = (xp + numpy.linalg.norm(rs)) * numpy.tan(ap)

    if (h>=k): # in sun
        return 1

    #e
    e = (xu - numpy.linalg.norm(rs)) * numpy.tan(au)

    if (e<=h): #in penumbra
        return (h-e)/k

    return 0 #in shadow

def solarpannelAngle(state, pannel):
    [_, _, _, _, positionSun] = state

    dot = numpy.dot(positionSun / numpy.linalg.norm(positionSun), pannel.dir)

    if dot <= 0:
        return 0

    return dot

def solarpannelLoss(state, pannel):
    return 0.8
